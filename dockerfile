#
# RUN Caddy RevProxy
# docker run -d --name caddy-revproxy -p 80:80 -p 443:443 -v caddy-revproxy-conf:/home/httpd/conf -v caddy-revproxy-certs:/home/httpd/.caddy itdengler/caddy [-email <email@address.com>]
#
# RUN Caddy RevProxy with Rancher networking
# docker run -d --name caddy-revproxy -p 80:80 -p 443:443 -v caddy-revproxy-conf:/home/httpd/conf -v caddy-revproxy-certs:/home/httpd/.caddy -l io.rancher.container.network=true itdengler/caddy [-email <email@address.com>]
# 
# RUN Docker-Gen 
# docker run -d --name caddy-revproxy-gen --volumes-from caddy-revproxy -v /var/run/docker.sock:/var/run/docker.sock:ro -v /usr/bin/docker:/usr/bin/docker itdengler/caddy-revproxy
#
# RUN a backend webserver container
# docker run -d -e VIRTUAL_HOST=example.com [-e VIRTUAL_PORT=8080] -v caddy-docroot:/home/httpd/public_html -v caddy-conf:/home/httpd/conf -v caddy-cert:/home/httpd/.caddy -v caddy-logs:/home/httpd/logs itdengler/caddy

FROM    alpine:latest
 
COPY	files/docker-gen /usr/local/bin/docker-gen
COPY	files/revproxy_reload.sh /usr/local/bin/revproxy_reload.sh
COPY    files/caddyfile.tmpl /etc/caddyfile.tmpl

ENTRYPOINT	[ "/usr/local/bin/docker-gen" ]
CMD		[ "-watch", "-notify=/usr/local/bin/revproxy_reload.sh", "/etc/caddyfile.tmpl", "/home/httpd/conf/caddyfile" ]
