#!/bin/sh

FILTER=${LABEL_FILTER:-"reverseproxy=web"}

docker kill --signal=USR1 `docker ps --filter label=$FILTER -q`
